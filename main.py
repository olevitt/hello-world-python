import uvicorn
from fastapi import FastAPI

app = FastAPI()


@app.get("/utilisateur")
def root():
    print("coucou")
    return [{"pseudo":"toto"},{"pseudo":"titi"}]


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
